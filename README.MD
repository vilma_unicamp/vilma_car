this is the package of the virtual vilma vehicle, it require the [morse simulator](https://www.openrobots.org/wiki/morse/), to install it, run the script `setup_install_morse.sh`. the script was made by Tiberio Ferreira, the project is in [github](https://github.com/tiberiusferreira/VilmaProject) with a lot of instruction and helps links.



The package has two node to work, the first one is the script to run morse call `run_morse.sh`. the second one, it is a script in python to control the car using the messages [ackermann_msgs](http://wiki.ros.org/ackermann_msgs), which send over a socket service the atuators values to the vilma car, rigth now it sendd the values: steering value, power_amount and velocity. the first is the value in radians of the steering, the second one define the acceleration or desacceleration of the car, and the velocity the bigger velocity to reach.

there is a launch file call `vilma_morse.launch` which execute the two script at the same time, so to start the simulator run

```
roslaunch vilma_morse vilma_morse.launch
```

The simulator is based in the CARINA II simulator in morse of USP- Sao carlos.


TODO: explain Vilma directory:
- **Default.py** Define the simulation parameters, frame by second, the world of the car and the global posistion of the car 

- **VilmaBuilder.py** define the sensors that the vilma car has, it is can be included GPS, camera, Pose, Twist, wheelspeed, and many more...

- **VilmaClass.py** it define the socket service of the vilma car: controlpower,steer , status, Brake ... 

- **vilma.blend** it define the geometry and the script with the phisic system of the car

- **city7.blend** it define world of the simulation.

- **/src/Vilma/sensors/wheelspeed.py** implement the wheel speed sensor of the four tires.

- **/src/Vilma/sensors/wheelspeedPub.py** Publish the wheel speed to ros.


TODO: it is possible to send brake command , but the python script just send to command controlpower and steer.
TODO: integrate the QT front end made by Tiberio, in theory it should work without a problem, it is necessary to test[github](https://github.com/tiberiusferreira/VilmaProject).

TODO: the install of the script and the Vilma Directory has not been configured in CMakeLists.txt
TODO: test and insert all the sensors.
 

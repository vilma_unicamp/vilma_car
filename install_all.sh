#!/bin/bash


sudo apt-get install ros-indigo-axis-camera ros-indigo-robot-upstart ros-indigo-joy ros-indigo-orocos-toolchain python-pip ros-indigo-ackermann-msgs ros-indigo-geodesy
    pip install gspread
    pip install --upgrade oauth2client
    pip install PyOpenSSL
    pip install pycrypto
cd ..
git clone https://github.com/KumarRobotics/ublox
git clone  https://github.com/srv/viso2.git
git clone https://gitlab.com/vilma_unicamp/vilma_planning.git
git clone https://gitlab.com/vilma_unicamp/vilma_ma_ros.git
git clone https://gitlab.com/vilma_unicamp/vilma_localization.git
git clone https://gitlab.com/vilma_unicamp/vilma_real_time.git
git clone https://gitlab.com/vilma_unicamp/vilma_perception.git
git clone https://gitlab.com/vilma_unicamp/vilma_simulator.git
